image: ruby:3.1.2

stages:
  - test
  - build_container
  - test_container
  - generate_manifest
  - commit_and_push

services:
  - redis:latest
  - postgres:16.2

variables:
  POSTGRES_DEV_DB: feigo_db_1_development
  POSTGRES_PROD_DB: feigo_db_1_production
  IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
  DATABASE_URL: "postgresql://$FEIGO_DB_USER:$FEIGO_DB_KEY@$FEIGO_DB_URL:5432"

# Cache gems in between builds
cache:
  paths:
    - vendor/ruby

include:
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml
  - template: Jobs/SAST.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml

#Buid App Container and push to Gitlab registry
build_container:
  stage: build_container
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build  -t $IMAGE_TAG . --platform linux/amd64
    - docker images
    - docker push $IMAGE_TAG

#Test App Container
test_container:
  stage: test_container
  image: docker:latest
  dependencies:
    - build_container
  services:
    - name: docker:dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - export DATABASE_URL=$DATABASE_BASE_URL/$POSTGRES_DEV_DB
    - apk --no-cache add curl
  script:
    - docker pull $IMAGE_TAG
    - docker run -d -p 3000:3000 --name feigo-container-test -e $DATABASE_URL $IMAGE_TAG
    - sleep 30
    - docker ps
    - docker logs feigo-container-test | grep "Listening"
    - docker stop feigo-container-test
    - docker rm feigo-container-test

# Generate FluxCD Manifest
generate_manifest:
  stage: generate_manifest
  script:
    - echo "Generating FluxCD manifest file..."
    - mkdir -p manifest/prod
    - |
      cat > manifest/prod/feigo-app-deployment.yaml <<EOF
      apiVersion: apps/v1
      kind: Deployment
      metadata:
        name: feigo-app-prod-$CI_COMMIT_SHORT_SHA
        namespace: production
      labels:
        app: feigo-app-prod
      spec:
        replicas: 2
        strategy:
          type: RollingUpdate
          rollingUpdate:
            maxSurge: 1
            maxUnavailable: 50%
        selector:
          matchLabels:
            app: feigo-app-prod
        template:
          metadata:
            labels:
              app: feigo-app-prod
          spec:
            containers:
              - name: feigo-app-prod-$CI_COMMIT_SHORT_SHA
                image: registry.gitlab.com/feigotechsolutions/feigo-app:$CI_COMMIT_SHORT_SHA
                ports:
                - containerPort: 3000
                resources:
                  requests:
                    memory: "256Mi"
                    cpu: "1000m"
                  limits:
                    memory: "512Mi"
                    cpu: "2000m"
            restartPolicy: Always
            imagePullSecrets:
              - name: gitlab-registry-secret
      EOF
  artifacts:
    paths:
      - manifest/prod/feigo-app-deployment.yaml
  only:
    - main
  environment:
    name: production
      
# Commit and push the generated manifest
commit_and_push:
  stage: commit_and_push
  environment:
    name: production
  dependencies:
    - generate_manifest
  script:
    - git config --global user.email "$GIT_USER_EMAIL"
    - git config --global user.name "$GIT_USER_NAME"
    - git checkout $CI_COMMIT_REF_NAME
    - git status
    - cat manifest/prod/feigo-app-deployment.yaml
    - git add -f manifest/prod/feigo-app-deployment.yaml
    - git status
    - git commit -m "[ci skip] Update feigo-app-deployment.yaml with the latest generated manifest" || echo "No changes to commit"
    - git push https://oauth2:$GIT_PAT@gitlab.com/feigotechsolutions/feigo-app.git $CI_COMMIT_REF_NAME
  only:
    - main
